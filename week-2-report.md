# Week 2 Report

In the second week, we continue working towards 3.0.0 milestones. I helped:

- review PR [#1140](https://github.com/cbm755/octsympy/pull/1140),
  which fixes *sympref test fails when in read-only directory* [#1060](https://github.com/cbm755/octsympy/issues/1060)
  - create follow-up issue *CI: Catch issues similar to #&#8288;1060* [#1141](https://github.com/cbm755/octsympy/issues/1141)
  - create follow-up issue *sympref: Document the inherent insecurity of some IPC mechanisms (e.g. system)* [#1143](https://github.com/cbm755/octsympy/issues/1143)
  - create follow-up issue *Octave: Document the inherit security risks of tempname and system* [#1144](https://github.com/cbm755/octsympy/issues/1144)
  - create follow-up issue *Create temporary files w/o race conditions whenever possible* [#1145](https://github.com/cbm755/octsympy/issues/1145)

- review PR [#1139](https://github.com/cbm755/octsympy/pull/1139),
  which fixes *Ensure `sym(x, 'clear')` and `syms x clear` give an error* [#733](https://github.com/cbm755/octsympy/issues/733)
- create PR [#1146](https://github.com/cbm755/octsympy/pull/1146),
  which fixes *times: elementwise multiplication broken on SymPy 1.9* [#1109](https://github.com/cbm755/octsympy/issues/1109)
- create PR [#1147](https://github.com/cbm755/octsympy/pull/1147),
  which fixes *Insure left and right division is working properly with mldivide.m and mrdivide.m* [#1118](https://github.com/cbm755/octsympy/issues/1118)
- create PR [#1153](https://github.com/cbm755/octsympy/pull/1153),
  which fixes *Regression caused by cd263b3853482226dbff&#8288;e99486ff98c8445d6a63* [#1151](https://github.com/cbm755/octsympy/issues/1151) (regression introduced by PR [#1136](https://github.com/cbm755/octsympy/pull/1136) during week 1)

All sympy 1.9 test failures are now fixed. We enable tests for sympy 1.9 in CI (with or without pythonic) and everything now builds successfully. We also discover a regression introduced during week 1 and fix it immediately. Finally, after reviewing PR [#1140](https://github.com/cbm755/octsympy/pull/1140), a number of existing issues are discovered. We plan to postpone resolving these issues to a later date as these issues are not our main focus right now. As you can see from the progress summary below, nearly all pre-set goals for the **present** part are now completed. As a result, we will work on any issues blocking the 3.0.0 release instead next week. Of course, we may also work on some other issues as well if appropriate.

## Progress Summary

Below is a summary of our progress:

- (**Present**) [Week 1-4] work towards 3.0.0 milestones
    - ~[Week 1-2] either support or blacklist Sympy 1.9~
        - ~investigate if Sympy 1.9 should be blacklisted (see [#1109](https://github.com/cbm755/octsympy/issues/1109))~
        - ~support Sympy 1.9 if we do not blacklist it~
            - ~run tests and doctests to check for regressions~
            - ~fix all fixable regressions & mark all unfixable ones to `XFAIL`~
            - ~enable CI for Sympy 1.9~
    - ~[Week 1-2] support Sympy 1.10.1~ (non-pythonic test failure already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
        - ~run tests and doctests to check for regressions~
        - ~fix all fixable regressions & mark all unfixable ones to `XFAIL`~
        - ~enable CI for Sympy 1.10.1~
    - ~[Week 3-4] improve existing CI~
        - ~investigate if all travis CI tests were already ported to GitHub actions~
        - ~port remaining tests if make sense~
        - ~remove the travis CI file [#1072](https://github.com/cbm755/octsympy/issues/1072)~
        - ~CI: update doctests matrix after heaviside fixes [#1103](https://github.com/cbm755/octsympy/issues/1103)~ (already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
    - ~[Week 3-4] sympref test fails when in read-only directory [#1060](https://github.com/cbm755/octsympy/issues/1060)~
        - ~create temporary files in `TMPDIR` instead of the current directory~
    - ~[Week 3-4] output argument error handling change in Octave 7.1 [#1111](https://github.com/cbm755/octsympy/issues/1111)~ (already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
        - ~adjust tests based on Octave version~
    - [Week 4] release octsympy 3.0.0 if sufficient milestones are completed
