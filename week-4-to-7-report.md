# Week 4 to 7 Report

During the forth to seventh week, we release octsympy 3.0.0 and start fixing long-standing bugs. I attended:

- GSoC 2022: meeting @ Mon 2022-07-11
- GSoC 2022: meeting @ Mon 2022-07-18
- GSoC 2022: meeting @ Mon 2022-07-25

and helped:

- complete [milestones](https://github.com/cbm755/octsympy/milestone/8?closed=1) leading to the 3.0.0 release
  - create PR [#1174](https://github.com/cbm755/octsympy/pull/1174),
    which fixes *Should it be necessary to `clear all` before unloading a pkg?* [#1160](https://github.com/cbm755/octsympy/issues/1160)
  - create follow-up issue *release: Use modern cryptographic hash function instead of MD5* [#1180](https://github.com/cbm755/octsympy/issues/1180)

- start to *Investigate PKG_DEL scripts* [#1176](https://github.com/cbm755/octsympy/issues/1176)
- review PR [#1175](https://github.com/cbm755/octsympy/pull/1175),
  which *Relegate the on-close for emergencies*
- investigate *readme claims to need liboctave-dev on Ubuntu* [#1181](https://github.com/cbm755/octsympy/issues/1181)
- investigate *port our python executable detection to upstream octave and use it here* [#1188](https://github.com/cbm755/octsympy/issues/1188)
- create PR [#1190](https://github.com/cbm755/octsympy/pull/1190),
  which fixes the build failure in PR [#1187](https://github.com/cbm755/octsympy/pull/1187) and other typo
- review PR [#1185](https://github.com/cbm755/octsympy/pull/1185),
  which fixes *reduce scary-looking false positive in testsuite* [#1183](https://github.com/cbm755/octsympy/issues/1183)
- review PR [#1184](https://github.com/cbm755/octsympy/pull/1184),
  which implement *Explicit error handling on file ops*
- reply to user's question [*How do I create skew t and hodograph plots*](https://octave.discourse.group/t/how-do-i-create-skew-t-and-hodograph-plots/2916)
- create PR [#1192](https://github.com/cbm755/octsympy/pull/1192),
  which fixes *adjoint function for numerical input* [#1049](https://github.com/cbm755/octsympy/issues/1049)

I also:

- completed the phase 1 evaluation
- started co-maintaining octsympy (see [#129](https://github.com/gnu-octave/packages/pull/129))
- discussed with mentor [@cbm755](https://github.com/cbm755) about working fewer hours from the end of July to the beginning of August due to other commitments

From week 4 to week 7, we attended the weekly GSoC meeting to keep ourselves informed about progress of all GSoC participants. We completed all milestones blocking the 3.0.0 release. As a result, octsympy 3.0.0 was released, finishing the **present** part of our project. After that, we worked on the the **past** part of our project and started fixing long-standing bugs. We also completed the phase 1 evaluation, started co-maintaining octsympy and had a discussion with mentor about working fewer hours due to other commitments. Next week we will continue fixing long-standing bugs and any regressions caused by the 3.0.0 release.

## Progress Summary

Below is a summary of our progress:

- ~(**Present**) [Week 1-4] work towards 3.0.0 milestones~
    - ~[Week 1-2] either support or blacklist Sympy 1.9~
        - ~investigate if Sympy 1.9 should be blacklisted (see [#1109](https://github.com/cbm755/octsympy/issues/1109))~
        - ~support Sympy 1.9 if we do not blacklist it~
            - ~run tests and doctests to check for regressions~
            - ~fix all fixable regressions & mark all unfixable ones to `XFAIL`~
            - ~enable CI for Sympy 1.9~
    - ~[Week 1-2] support Sympy 1.10.1~ (non-pythonic test failure already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
        - ~run tests and doctests to check for regressions~
        - ~fix all fixable regressions & mark all unfixable ones to `XFAIL`~
        - ~enable CI for Sympy 1.10.1~
    - ~[Week 3-4] improve existing CI~
        - ~investigate if all travis CI tests were already ported to GitHub actions~
        - ~port remaining tests if make sense~
        - ~remove the travis CI file [#1072](https://github.com/cbm755/octsympy/issues/1072)~
        - ~CI: update doctests matrix after heaviside fixes [#1103](https://github.com/cbm755/octsympy/issues/1103)~ (already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
    - ~[Week 3-4] sympref test fails when in read-only directory [#1060](https://github.com/cbm755/octsympy/issues/1060)~
        - ~create temporary files in `TMPDIR` instead of the current directory~
    - ~[Week 3-4] output argument error handling change in Octave 7.1 [#1111](https://github.com/cbm755/octsympy/issues/1111)~ (already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
        - ~adjust tests based on Octave version~
    - ~[Week 4] release octsympy 3.0.0 if sufficient milestones are completed~

- (**Past**) [Week 5-8] fix long-standing bugs
    - [Week 5-6] add cart2pol function and friends [#865](https://github.com/cbm755/octsympy/issues/865)
    - [Week 5-6] support sortrows [#362](https://github.com/cbm755/octsympy/issues/362)
    - ~[Week 5-6] adjoint function for numerical input [#1049](https://github.com/cbm755/octsympy/issues/1049)~
    - ~[Week 7-8] ztrans [#853](https://github.com/cbm755/octsympy/issues/853)~ (fixed by me, the GSoC participant, before GSoC had officially started)
        - ~use the series definition of [(unilateral) z-transform](https://en.wikipedia.org/wiki/Z-transform#Unilateral_Z-transform)~
    - [Week 7-8] Piecewise Support [#582](https://github.com/cbm755/octsympy/issues/582)
    - [Week 8] fix additional bugs depending on our progress

- ~[Week 8] phase 1 report~ (should be in week 7 rather than week 8)
