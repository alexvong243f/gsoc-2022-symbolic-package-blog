# Week 1 Report

In the first week, we have been working towards 3.0.0 milestones. I helped:

- review PR [#1131](https://github.com/cbm755/octsympy/pull/1131),
  which fixes *Bump minimum Octave to 5.1* [#1074](https://github.com/cbm755/octsympy/issues/1074)
- create PR [#1135](https://github.com/cbm755/octsympy/pull/1135), which fixes *Let's collect more info in `sympref diagnose`* [#1127](https://github.com/cbm755/octsympy/issues/1127)
- create PR [#1136](https://github.com/cbm755/octsympy/pull/1136),
  which fixes *CI: enable pythonic tests* [#1067](https://github.com/cbm755/octsympy/issues/1067)
- create PR [#1137](https://github.com/cbm755/octsympy/pull/1137),
  which fixes *update README to not reference travis anymore* [#1132](https://github.com/cbm755/octsympy/issues/1132)
- investigate *times: elementwise multiplication broken on SymPy 1.9* [#1109](https://github.com/cbm755/octsympy/issues/1109)
- investigate *Insure left and right division is working properly with mldivide.m and mrdivide.m* [#1118](https://github.com/cbm755/octsympy/issues/1118)

Basically, we ported all pythonic tests in the legacy travis CI file to use github action. We managed to make everything build with sympy 1.10.1 after fixing all test failure. In the next week, we will try to fix sympy 1.9 test failure and make everything build with sympy 1.9 with or without pythonic.

## Progress Summary

Below is a summary of our progress:

- (**Present**) [Week 1-4] work towards 3.0.0 milestones
    - [Week 1-2] either support or blacklist Sympy 1.9
        - ~investigate if Sympy 1.9 should be blacklisted (see [#1109](https://github.com/cbm755/octsympy/issues/1109))~
        - support Sympy 1.9 if we do not blacklist it
            - run tests and doctests to check for regressions
            - fix all fixable regressions & mark all unfixable ones to `XFAIL`
            - enable CI for Sympy 1.9
    - ~[Week 1-2] support Sympy 1.10.1~ (non-pythonic test failure already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
        - ~run tests and doctests to check for regressions~
        - ~fix all fixable regressions & mark all unfixable ones to `XFAIL`~
        - ~enable CI for Sympy 1.10.1~
    - ~[Week 3-4] improve existing CI~
        - ~investigate if all travis CI tests were already ported to GitHub actions~
        - ~port remaining tests if make sense~
        - ~remove the travis CI file [#1072](https://github.com/cbm755/octsympy/issues/1072)~
        - ~CI: update doctests matrix after heaviside fixes [#1103](https://github.com/cbm755/octsympy/issues/1103)~ (already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
    - [Week 3-4] sympref test fails when in read-only directory [#1060](https://github.com/cbm755/octsympy/issues/1060)
        - create temporary files in `TMPDIR` instead of the current directory
    - [Week 3-4] output argument error handling change in Octave 7.1 [#1111](https://github.com/cbm755/octsympy/issues/1111)
        - adjust tests based on Octave version
    - [Week 4] release octsympy 3.0.0 if sufficient milestones are completed
