# Week 12 Report

In the twelfth week, we finish fixing long-standing bugs and continue working on removing non-`Expr` from `Matrix`. I attended:

- GSoC 2022: meeting @ Mon 2022-08-29

and helped:

- create PR [#1214](https://github.com/cbm755/octsympy/pull/1214),
  which fixes *Piecewise Support* [#582](https://github.com/cbm755/octsympy/issues/582)
- create WIP PR [#1234](https://github.com/cbm755/octsympy/pull/1234),
  which *Add new function* `@sym/iztrans`

- create and review PRs to be merged onto the `Array_not_Matrix` branch
  - create PR [#1212](https://github.com/cbm755/octsympy/pull/1212),
    which fixes *pythonic: add `dbout` command* [#1210](https://github.com/cbm755/octsympy/issues/1210)
  - create PR [#1213](https://github.com/cbm755/octsympy/pull/1213),
    which *makes the `mat_rclist_*` functions `Array`-compatible*
    and fixes *common helper python function for Array/Matrix return* [#1211](https://github.com/cbm755/octsympy/issues/1211)
  - create PR [#1216](https://github.com/cbm755/octsympy/pull/1216),
    which *makes `@sym/vertcat`, `@sym/horzcat` and `@sym/transpose` Array-compatible*
  - create follow-up issue _@sym/{ctranspose,transpose}: *transpose does not work on None_ [#1215](https://github.com/cbm755/octsympy/issues/1215)
  - create PR [#1219](https://github.com/cbm755/octsympy/pull/1219),
    which *re-implements `@sym/repmat` without using `pycall_sympy__`*
    and fixes *@sym/repmat: Not outputing empty matrices the same as repmat* [#1218](https://github.com/cbm755/octsympy/issues/1218)
  - review and improve PR [#1223](https://github.com/cbm755/octsympy/pull/1223),
    which fixes *Array_not_Matrix: boolean ops on Arrays are broken* [#1222](https://github.com/cbm755/octsympy/issues/1222)
  - review PR [#1224](https://github.com/cbm755/octsympy/pull/1224),
    which fixes *isequal: failing with error: cell2mat: C must be a cell array* [#1221](https://github.com/cbm755/octsympy/issues/1221)
  - review PR [#1227](https://github.com/cbm755/octsympy/pull/1227),
    which fixes *subs: needs to use make_matrix_or_array* [#1226](https://github.com/cbm755/octsympy/issues/1226)
    and *An Matrix can be incorrectly expanded into an Array* [#1228](https://github.com/cbm755/octsympy/issues/1228)
  - create PR [#1230](https://github.com/cbm755/octsympy/pull/1230),
    which *Use sympy function 'unflatten'* in `@sym/piecewise`
  - create PR [#1231](https://github.com/cbm755/octsympy/pull/1231),
    which *Use sympy functions and generalise `make_matrix_or_array`*
  - create WIP PR [#1235](https://github.com/cbm755/octsympy/pull/1235),
    which *Add 2D sym functions and use them*

This week we attended the GSoC meeting to keep ourselves informed about the progress of all GSoC participants. We finished fixing all long-standing bugs in the updated timeline except *support sortrows* [#362](https://github.com/cbm755/octsympy/issues/362), which was skipped since `@sym/sortrows` was not requested as much as other functions on the list, completing the **past** subproject. More importantly, we continued working on the optional but more urgent *future* subproject. We introduced a helper function to construct 2D sym, which was our way to work around the lack of [sum type](https://en.wikipedia.org/wiki/Tagged_union) related features in Python. We made use of this function extensively and made considerable progress in getting non-`Expr` out of `Matrix`. For examples, functions such as `@sym/mat_rclist_*`, `@sym/*cat`, `@sym/transpose` and `@sym/repmat` now place non-`Expr` in `Array` rather than `Matrix`.

Next week we will work on the final report, which will reflect on what we have done so far, highlight challenges and lay out future plans.

## Progress Summary

Below is a summary of our progress:

- ~(**Past**) [Week 5-8] fix long-standing bugs~ (all fixed except [#362](https://github.com/cbm755/octsympy/issues/362), skipped due to *future* subproject being more urgent)
    - ~[Week 5-6] add cart2pol function and friends [#865](https://github.com/cbm755/octsympy/issues/865)~ (`@sym/cart2pol` added by me, the GSoC participant)
    - [Week 5-6] support sortrows [#362](https://github.com/cbm755/octsympy/issues/362)
    - ~[Week 5-6] adjoint function for numerical input [#1049](https://github.com/cbm755/octsympy/issues/1049)~
    - ~[Week 7-8] ztrans [#853](https://github.com/cbm755/octsympy/issues/853)~ (fixed by me, the GSoC participant, before GSoC had officially started)
        - ~use the series definition of [(unilateral) z-transform](https://en.wikipedia.org/wiki/Z-transform#Unilateral_Z-transform)~
    - ~[Week 7-8] Piecewise Support [#582](https://github.com/cbm755/octsympy/issues/582)~
    - ~[Week 8] fix additional bugs depending on our progress~ (`@sym/iztrans` almost done except lacking help text and tests, see WIP PR [#1234](https://github.com/cbm755/octsympy/pull/1234))

- ~\<Optional\> (*Future*) [Week 9-12] make octsympy easier to maintain~
    - ~[Week 9-12] get non-expr out of matrix as much as possible since sympy deprecated the use of non-expr in matrix~ (considerable progress made, see [#1124](https://github.com/cbm755/octsympy/issues/1124))
        - ~[Week 9-10] investigate draft PR [#1194](https://github.com/cbm755/octsympy/pull/1194)~
        - ~[Week 9-10] investigate *laplace test giving deprecation warning on SymPy > 1.9* [#1052](https://github.com/cbm755/octsympy/issues/1052)~
        - ~[Week 9-10] investigate *eq: giving TypeError: cannot determine truth value of Relational* [#1055](https://github.com/cbm755/octsympy/issues/1055)~ (fixed by mentor [@cbm755](https://github.com/cbm755))
        - ~[Week 11-12] fix *laplace test giving deprecation warning on SymPy > 1.9* [#1052](https://github.com/cbm755/octsympy/issues/1052)~ (warning no longer exists on main branch but one should investigate [#21504](https://github.com/sympy/sympy/issues/21504) before closing)
        - [Week 11-12] fix *tracker issue for non-Expr in matrices* [#1124](https://github.com/cbm755/octsympy/issues/1124) if time allow

- [Week 13] final report
