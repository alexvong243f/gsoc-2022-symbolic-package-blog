## TODO Deliverables

- (**Present**) [Week 1-4] work towards 3.0.0 milestones
    - [Week 1-2] either support or blacklist Sympy 1.9
        - investigate if Sympy 1.9 should be blacklisted (see [#1109](https://github.com/cbm755/octsympy/issues/1109))
        - support Sympy 1.9 if we do not blacklist it
            - run tests and doctests to check for regressions
            - fix all fixable regressions & mark all unfixable ones to `XFAIL`
            - enable CI for Sympy 1.9
    - [Week 1-2] support Sympy 1.10.1
        - run tests and doctests to check for regressions
        - fix all fixable regressions & mark all unfixable ones to `XFAIL`
        - enable CI for Sympy 1.10.1
    - [Week 3-4] improve existing CI
        - investigate if all travis CI tests were already ported to GitHub actions
        - port remaining tests if make sense
        - remove the travis CI file [#1072](https://github.com/cbm755/octsympy/issues/1072)
        - CI: update doctests matrix after heaviside fixes [#1103](https://github.com/cbm755/octsympy/issues/1103)
    - [Week 3-4] sympref test fails when in read-only directory [#1060](https://github.com/cbm755/octsympy/issues/1060)
        - create temporary files in `TMPDIR` instead of the current directory
    - [Week 3-4] output argument error handling change in Octave 7.1 [#1111](https://github.com/cbm755/octsympy/issues/1111)
        - adjust tests based on Octave version
    - [Week 4] release octsympy 3.0.0 if sufficient milestones are completed

- (**Past**) [Week 5-8] fix long-standing bugs
    - [Week 5-6] add cart2pol function and friends [#865](https://github.com/cbm755/octsympy/issues/865)
    - [Week 5-6] support sortrows [#362](https://github.com/cbm755/octsympy/issues/362)
    - [Week 5-6] adjoint function for numerical input [#1049](https://github.com/cbm755/octsympy/issues/1049)
    - [Week 7-8] ztrans [#853](https://github.com/cbm755/octsympy/issues/853)
        - use the series definition of [(unilateral) z-transform](https://en.wikipedia.org/wiki/Z-transform#Unilateral_Z-transform)
    - [Week 7-8] Piecewise Support [#582](https://github.com/cbm755/octsympy/issues/582)
    - [Week 8] fix additional bugs depending on our progress

- ~[Week 8]~ [Week 7] phase 1 report (should be in week 7 rather than week 8)

- \<Optional\> (*Future*) [Week 9-12] make octsympy easier to maintain (*Future* part changed after discussion with mentor [@cbm755](https://github.com/cbm755), we can still work on the original plan if time allows but this seems unlikely to me)
    - ~[Week 9-10] disable `popen2` and `system` IPC mechanism to simplify debugging~
    - ~[Week 9-10] create a new experimental `xsym` (classdef) class acting as a thin wrapper for underlying Sympy values~
    - ~[Week 9-10] incrementally update functions to support `xsym` values~
        - ~only octsympy functions and primitive `py.sympy` methods should be used when dealing with `xsym` values~
        - ~`pycall_sympy__` should never be called when dealing with `xsym` values~
    - ~[Week 11-12] investigate what is the best way to expose the full functionality of Sympy with less glue and create a proof-of-concept~
        - ~we should focus on user extensibility and make it easy for users to define their own wrapper around Sympy functions, methods or constants~
        - ~probably we need a way to get the underlying Sympy value out of a `xsym` value~
    - [Week 9-12] get non-expr out of matrix as much as possible since sympy deprecated the use of non-expr in matrix (see [#1124](https://github.com/cbm755/octsympy/issues/1124))
      - [Week 9-10] investigate draft PR [#1194](https://github.com/cbm755/octsympy/pull/1194)
      - [Week 9-10] investigate *laplace test giving deprecation warning on SymPy > 1.9* [#1052](https://github.com/cbm755/octsympy/issues/1052)
      - [Week 9-10] investigate *eq: giving TypeError: cannot determine truth value of Relational* [#1055](https://github.com/cbm755/octsympy/issues/1055)
      - [Week 11-12] fix *laplace test giving deprecation warning on SymPy > 1.9* [#1052](https://github.com/cbm755/octsympy/issues/1052)
      - [Week 11-12] fix *tracker issue for non-Expr in matrices* [#1124](https://github.com/cbm755/octsympy/issues/1124) if time allow

- [Week 13] final report
