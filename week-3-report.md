# Week 3 Report

In the third week, we work on issues blocking the 3.0.0 release. I attended:

- [Online Developer Meeting (2022-06-28)](https://wiki.octave.org/Online_Developer_Meeting_(2022-06-28))
- GSoC 2022: meeting @ Fri 2022-07-01

and helped:

- investigate *Get `pkg test symbolic` working* [#1142](https://github.com/cbm755/octsympy/issues/1142)
  - create follow-up [bug #62681](https://savannah.gnu.org/bugs/index.php?62681): pkg test: Tests outside of the specified package are being run

- investigate *error: ignoring const execution_exception& while preparing to exit* [#1163](https://github.com/cbm755/octsympy/issues/1163)
- review PR [#1158](https://github.com/cbm755/octsympy/pull/1158),
  which fixes *CI: exercise more "pkg" commands* [#1156](https://github.com/cbm755/octsympy/issues/1156)
- investigate *CI: still seeing "fatal: unsafe repository"* [#1161](https://github.com/cbm755/octsympy/issues/1161)
- create PR [#1167](https://github.com/cbm755/octsympy/pull/1167),
  which fixes *sym/ztrans references non-implemented function* [#1122](https://github.com/cbm755/octsympy/issues/1122)
- create PR [#1165](https://github.com/cbm755/octsympy/pull/1165),
  which fixes *CI: run a "make html" job* [#1157](https://github.com/cbm755/octsympy/issues/1157)

This week we attended the (monthly) Octave development meeting and the (weekly) GSoC meeting to keep ourselves informed about the latest Octave development and the GSoC projects of all participants. Besides, we investigated and fixed multiple issues blocking the 3.0.0 release. Next week we will work on remaining issues blocking the 3.0.0 release (if any) and release octsympy 3.0.0 (if appropriate).

## Progress Summary

Below is a summary of our progress:

- (**Present**) [Week 1-4] work towards 3.0.0 milestones
    - ~[Week 1-2] either support or blacklist Sympy 1.9~
        - ~investigate if Sympy 1.9 should be blacklisted (see [#1109](https://github.com/cbm755/octsympy/issues/1109))~
        - ~support Sympy 1.9 if we do not blacklist it~
            - ~run tests and doctests to check for regressions~
            - ~fix all fixable regressions & mark all unfixable ones to `XFAIL`~
            - ~enable CI for Sympy 1.9~
    - ~[Week 1-2] support Sympy 1.10.1~ (non-pythonic test failure already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
        - ~run tests and doctests to check for regressions~
        - ~fix all fixable regressions & mark all unfixable ones to `XFAIL`~
        - ~enable CI for Sympy 1.10.1~
    - ~[Week 3-4] improve existing CI~
        - ~investigate if all travis CI tests were already ported to GitHub actions~
        - ~port remaining tests if make sense~
        - ~remove the travis CI file [#1072](https://github.com/cbm755/octsympy/issues/1072)~
        - ~CI: update doctests matrix after heaviside fixes [#1103](https://github.com/cbm755/octsympy/issues/1103)~ (already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
    - ~[Week 3-4] sympref test fails when in read-only directory [#1060](https://github.com/cbm755/octsympy/issues/1060)~
        - ~create temporary files in `TMPDIR` instead of the current directory~
    - ~[Week 3-4] output argument error handling change in Octave 7.1 [#1111](https://github.com/cbm755/octsympy/issues/1111)~ (already fixed by mentor [@cbm755](https://github.com/cbm755) before GSoC had officially started)
        - ~adjust tests based on Octave version~
    - [Week 4] release octsympy 3.0.0 if sufficient milestones are completed
