# GSoC 2022 Symbolic package project blog

This is the public blog for the Google Summer of Code 2022 Symbolic package project.

You can read the project's goals as stated in the [proposal](./proposal.md). Please note that the timeline in **TODO Deliverables** is now outdated, see [updated timeline](./updated-timeline.md) for the latest timeline.

Reports about the project will be provided below.

## Final Report

- [Final Report](./final-report.md)

## Previous Reports

- [Week 1 Report](./week-1-report.md)
- [Week 2 Report](./week-2-report.md)
- [Week 3 Report](./week-3-report.md)
- [Week 4 to 7 Report](./week-4-to-7-report.md)
- [Week 8 to 11 Report](./week-8-to-11-report.md)
- [Week 12 Report](./week-12-report.md)

## Copyright

All files in this repository are authored by Alex Vong and are licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.
