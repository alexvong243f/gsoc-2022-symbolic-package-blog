# GSoC 2022 Symbolic package project final report

This is the final report for the Google Summer of Code 2022 Symbolic package project. See [README](./README.md) for other documents.

**TL;DR:** See section [Merged Commits](./final-report.md#merged-commits) for what was merged and [Unmerged Commits](./final-report.md#unmerged-commits) for what was not merged.

## Table of Contents

[[_TOC_]]

## Introduction

Our project consisted of 3 parts. The **present** subproject was about completing milestones leading to the 3.0.0 release. The **past** subproject was about fixing long-standing bugs. The optional *future* subproject was about improving maintainance going forward.

## The Present Subproject

To complete the **present** subproject, we investigated and fixed multiple CI test failures, ported all pythonic tests from travis CI to github action, made CI work with the latest sympy (1.9 and 1.10.1) and latest Octave (7.1.0) at that moment, started building documentation in CI, fixed cleanup warning in CI, etc. After completing these milestones, the **present** subproject was completed and octsympy 3.0.0 was released.

Details regarding what was done can be found in:

- [Week 1 Report](./week-1-report.md)
- [Week 2 Report](./week-2-report.md)
- [Week 3 Report](./week-3-report.md)
- [Week 4 to 7 Report](./week-4-to-7-report.md)

---

After the release of octsympy 3.0.0, an important regression was reported. As a result, we did the bug-fix release octsympy 3.0.1 and completed various post-release tasks as well.

Details regarding what was done can be found in:

- [Week 8 to 11 Report](./week-8-to-11-report.md)

## The Past Subproject

To complete the **past** subproject, we added several new functions requested by users, such as `@sym/ztrans`, `@double/adjoint`, `@sym/cart2pol`, `@sym/piecewise`, etc. After adding these functions, all long-standing bugs on the updated timeline except *support sortrows* [#362](https://github.com/cbm755/octsympy/issues/362) were fixed, finishing the **past** subproject. We skipped adding `@sym/sortrows` because it was not requested as much as other functions and the *future* subproject was more urgent. In addition, we created a WIP PR [#1234](https://github.com/cbm755/octsympy/pull/1234) for adding the new function `@sym/iztrans`.

Details regarding what was done can be found in:

- [Week 4 to 7 Report](./week-4-to-7-report.md)
- [Week 8 to 11 Report](./week-8-to-11-report.md)
- [Week 12 Report](./week-12-report.md)

## The Future Subproject

To complete the optional *future* subproject, we investigated *tracker issue for non-Expr in matrices* [#1124](https://github.com/cbm755/octsympy/issues/1124) and draft PR [#1194](https://github.com/cbm755/octsympy/pull/1194). After that, we came up with plans to gradually get rid of non-`Expr` from `Matrix`. First, we introduced a helper function for constructing 2D sym. This was our way to work around the lack of [sum type](https://en.wikipedia.org/wiki/Tagged_union) related features in Python. Then, we made use of this function extensively and updated functions such as `@sym/mat_rclist_*`, `@sym/*cat`, `@sym/transpose`, `@sym/repmat`, etc, to place non-`Expr` in `Array` rather than `Matrix`. After considerable progress was made in getting rid of non-`Expr` from `Matrix`, we considered the *future* subproject to be completed.

Details regarding what was done can be found in:

- [Week 8 to 11 Report](./week-8-to-11-report.md)
- [Week 12 Report](./week-12-report.md)

## Workflow

We mostly followed the workflow described in section [**MANAGING BRANCHES**](https://git-scm.com/docs/gitworkflows#_managing_branches) of [**gitworkflows**(7)](https://git-scm.com/docs/gitworkflows). For each proposed change, we create a topic branch and test it. If the proposed change does not cause any test failures, we will create a pull request (PR) and the PR will be reviewed. If the PR looks good, it will be merged onto the `main` branch, which is the main development branch. If not, modification will have to be made. For non-trivial modification, another review is needed.

## Merged Commits

Merged commits are either merged onto `main` which is the main development branch, or `Array_not_Matrix` which is the staging branch for works related to getting rid of non-`Expr` in `Matrix`. To obtain them, one can simply clone the repository:

```
$ git clone https://github.com/alexvong1995/octsympy.git
```

and checkout the relevant branches.

After that, we can generate a graph of merged commits:

```
$ git log --graph --pretty=oneline \
      --decorate --decorate-refs=refs/remotes/origin --decorate-refs=refs/tags \
      --author='Alex Vong' --until=2022-09-12 \
      origin/main origin/Array_not_Matrix \
      ^e57a57d19309a2d8039bd1de640fa20f645cd712~1 | \
      sed -e 's@\(.*\) \([0-9a-f]\{8\}\)\([0-9a-f]\{32\}\) \(.*\)@`\1` [\2](https://github.com/cbm755/octsympy/commit/\2\3) `\4`<br>@' \
          -e 's@^[^`][^`]*$@`&`<br>@'
```

`*  ` [0327827a](https://github.com/cbm755/octsympy/commit/0327827ac539a18e8a42f81dc17da8acf5f7ca94) `(origin/Array_not_Matrix) Merge branch 'use-sympy-funcs-and-generalise' into Array_not_Matrix`<br>
`|\  `<br>
`| *` [59176a2d](https://github.com/cbm755/octsympy/commit/59176a2d17418c5288ae07f5052c2abba9eb1a1e) `make_matrix_or_array: Generalise to accept an iterable of iterables.`<br>
`| *` [05fbad67](https://github.com/cbm755/octsympy/commit/05fbad67d281179ba44920e6d7d856a23a1ab7fa) `Merge branch 'use-sympy-funcs' into use-sympy-funcs-and-generalise`<br>
`|/| `<br>
`| *` [321ee170](https://github.com/cbm755/octsympy/commit/321ee1709f011ea568a243d5f8722fa6e3a14db9) `@sym/private/mat_replace: Make matrix mutable before modifying.`<br>
`| *` [c9f6f0f9](https://github.com/cbm755/octsympy/commit/c9f6f0f9b1d0edaee5c29ce7d9e4ae2ece8708d4) `@sym/transpose: Use sympy function 'transpose'.`<br>
`| *` [1f8163e0](https://github.com/cbm755/octsympy/commit/1f8163e0e2c948fd55641facac83064c9e69dfba) `@sym/private/mat_rclist_asgn: Use sympy function 'flatten'.`<br>
`| *` [bd795e8a](https://github.com/cbm755/octsympy/commit/bd795e8a6bd2547e24e6c6df62e4f2952a0fb3b6) `@sym/vertcat: Use sympy function 'flatten'.`<br>
`* |` [526fe237](https://github.com/cbm755/octsympy/commit/526fe2377e41854ea48ee1b4cef8b2e48ffce40d) `Merge branch 'subs' into Array_not_Matrix`<br>
` /  `<br>
`| *` [a82cbd88](https://github.com/cbm755/octsympy/commit/a82cbd88c57cfb4a08a4fcb419d2bf99a24a9409) `Merge branch '2dloop' into Array_not_Matrix`<br>
`| *` [37724d62](https://github.com/cbm755/octsympy/commit/37724d624c6952669709c85e058f3e0fc72b604c) `private/python_ipc_native: Disable 'dbg_no_array' to keep in sync...`<br>
`| *  ` [e895cc7f](https://github.com/cbm755/octsympy/commit/e895cc7f7a62f6449335b9c3ba711bfaedd6e59a) `Merge branch 'repmat-without-pycall' into Array_not_Matrix`<br>
`| |\  `<br>
`| |/  `<br>
`|/|   `<br>
`* |` [65da8ab6](https://github.com/cbm755/octsympy/commit/65da8ab60c3a7b46a285697e20a1425ced570e6b) `@sym/repmat: Re-implement function without using 'pycall_sympy__'.`<br>
`* |` [fb044e38](https://github.com/cbm755/octsympy/commit/fb044e387ddbe5c25d419928de0100d332b3388c) `@sym/private/mat_rclist_{access,asgn}: Remove unused variables.`<br>
`| *  ` [9a3b3f00](https://github.com/cbm755/octsympy/commit/9a3b3f00b923254fb9d1aec14c45c15a2c58d188) `Merge branch 'make-cats-array-compat' into Array_not_Matrix`<br>
`| |\  `<br>
`| |/  `<br>
`|/|   `<br>
`* |` [0d936027](https://github.com/cbm755/octsympy/commit/0d9360275c10a2b6f2cd57e4c56c84b64ddc8d13) `@sym/horzcat: Make function Array-compatible by simplifying it.`<br>
`* |` [136184d6](https://github.com/cbm755/octsympy/commit/136184d6df506bc9945d8f341e9b46691895c5b4) `@sym/transpose: Make function Array-compatible.`<br>
`* |` [d3b15473](https://github.com/cbm755/octsympy/commit/d3b154731803c95460af08722e7a62a718d20870) `@sym/vertcat: Make function Array-compatible.`<br>
`* |` [f1b210fd](https://github.com/cbm755/octsympy/commit/f1b210fdf07f92ce03edee5fa6f2564d343b03be) `@sym/{horzcat,vertcat}: Fix typo.`<br>
`| *` [5385713c](https://github.com/cbm755/octsympy/commit/5385713ccf34534366de6838691359e3ecca4065) `Merge branch 'test-array-in-mat_rclist_' into Array_not_Matrix`<br>
`|/  `<br>
`*` [54c82719](https://github.com/cbm755/octsympy/commit/54c827199578e49cc24eae99a6202d6c6861d325) `@sym/private/mat_rclist_asgn.m: Test Array-compatible code.`<br>
`*` [53059ac8](https://github.com/cbm755/octsympy/commit/53059ac82043d4a472a997aab07072a1b34e91f3) `@sym/private/mat_rclist_access.m: Test Array-compatible code.`<br>
`*` [b618e5fd](https://github.com/cbm755/octsympy/commit/b618e5fdd0bb1080d913d965ed4040e6f1f6bb6a) `Python header: Add new function 'make_matrix_or_array'.`<br>
`*` [90608c1f](https://github.com/cbm755/octsympy/commit/90608c1f5ec4ecfa764c4e7698c717d842dcdc4a) `@sym/piecewise: Use sympy function 'unflatten'.`<br>
`*` [1ab5ce74](https://github.com/cbm755/octsympy/commit/1ab5ce74ab6f9074a79c6ecd094d5c2f664789f8) `@sym/piecewise: Remove redundant import.`<br>
`*` [863c02cf](https://github.com/cbm755/octsympy/commit/863c02cff5a247b2da718fcb1d2cbc085181debe) `@sym/piecewise: Add new function.`<br>
`*` [47b842b9](https://github.com/cbm755/octsympy/commit/47b842b91effddf569de185fd625e3a3d4c2c190) `private/python_ipc_native: Add new function 'dbout'.`<br>
`*` [0866db00](https://github.com/cbm755/octsympy/commit/0866db0023946a82f50521c6b2e11949205012a0) `@sym/cart2pol: Add new function.`<br>
`*  ` [1252eec1](https://github.com/cbm755/octsympy/commit/1252eec1d43ffc2f0fc0d0d731e18718781f2498) `Merge branch 'post-3.0.1-bump' (PR #1205)`<br>
`|\  `<br>
`| *` [049290e6](https://github.com/cbm755/octsympy/commit/049290e69ef6bbc0ba4604bdd289b150d5b974c1) `DESCRIPTION, NEWS, inst/sympref.m: Post release bump (3.0.1+).`<br>
`|/  `<br>
`*  ` [5512db9a](https://github.com/cbm755/octsympy/commit/5512db9a226d4f738e5062904d26f7b0d024b4a6) `(tag: v3.0.1) Merge branch 'release-v3.0.1' (PR #1202)`<br>
`|\  `<br>
`| *` [6dd372c6](https://github.com/cbm755/octsympy/commit/6dd372c68eb84cfaf83fb6e916839702d7dff113) `DESCRIPTION, NEWS, inst/sympref.m: Bump version to '3.0.1'.`<br>
`| *` [51560010](https://github.com/cbm755/octsympy/commit/51560010e8028ced7226adb007b727254694a00b) `NEWS: Add news entry.`<br>
`|/  `<br>
`*` [83a07ed2](https://github.com/cbm755/octsympy/commit/83a07ed23fca1bbe8f61bb7090edd0f75b9abf58) `Merge branch 'fixes-1009'`<br>
`*` [3b99273e](https://github.com/cbm755/octsympy/commit/3b99273e35d95c882a450131bc9fafbaa9c3a942) `sympref diagnose: Detect if Python is running in a Cygwin-like...`<br>
`*` [ef672538](https://github.com/cbm755/octsympy/commit/ef672538dbc14a19f59e2636e55168261faa9771) `sympref diagnose: Show full path of python executable when using...`<br>
`*` [0412dc2e](https://github.com/cbm755/octsympy/commit/0412dc2effddb726cbaa70a370aa24cab66fe0ff) `CI: Run tests on Octave 7.2.0.`<br>
`*` [ab7ae23a](https://github.com/cbm755/octsympy/commit/ab7ae23a438f94711c97c1c4ec79fba5b6ab9eb4) `CI: Add missing 'apt-get update'.`<br>
`*` [6d1f1773](https://github.com/cbm755/octsympy/commit/6d1f177319e368692f0fa33beb1d7cddeb402850) `private/python_ipc_system: Convert to Cygwin path when needed.`<br>
`*` [a00f2e9e](https://github.com/cbm755/octsympy/commit/a00f2e9ecbb2f06af78ea418b43cf000756f3cda) `@double/adjoint: Add new function.`<br>
`*` [8ad976eb](https://github.com/cbm755/octsympy/commit/8ad976ebef21d09727a0f425a495a48b2e52a212) `CI: Fix typo.`<br>
`*` [b39c768c](https://github.com/cbm755/octsympy/commit/b39c768cf874b730398dab4890332a851455bf81) `private/python_ipc_popen2: Fixes warning after unloading package.`<br>
`*` [9aef0381](https://github.com/cbm755/octsympy/commit/9aef038139188ec86d77084fa7fd6a596a361931) `CI: Build HTML documentation.`<br>
`*` [cdcd2097](https://github.com/cbm755/octsympy/commit/cdcd2097ae3b48d72bf54fb539c4a73ed4c6d89b) `@sym/ztrans: Temporarily remove reference to unimplemented function.`<br>
`*` [aba48eb2](https://github.com/cbm755/octsympy/commit/aba48eb2cadefed8cc5e9134ff1339d59c08f627) `private/store_vars_in_python: Fixes regression...`<br>
`*` [b58665a8](https://github.com/cbm755/octsympy/commit/b58665a897c45bd33448f10cdb7d4fce3a463708) `@sym/mrdivide: Simplify implementation.`<br>
`*` [d6da7b8b](https://github.com/cbm755/octsympy/commit/d6da7b8bee01cc9494aeea707fc8e3a1664f47de) `CI: Enable fail-fast for pythonic tests.`<br>
`*` [1f8d057f](https://github.com/cbm755/octsympy/commit/1f8d057fa6d32178a6dc0597c065d801b74da3c0) `CI: Enable tests for sympy 1.9.`<br>
`*` [5c536d1f](https://github.com/cbm755/octsympy/commit/5c536d1f73eb04822fcc6c038b82f2fc6655ef51) `inst/@sym/{rdivide,times}: Use workaround for sympy <1.9.`<br>
`| *` [7ca01378](https://github.com/cbm755/octsympy/commit/7ca01378be73da790a8306e96574567e295f7e8f) `sympref diagnose: Show information about the current system.`<br>
`| *` [117a5062](https://github.com/cbm755/octsympy/commit/117a50625c78d88e43565b6fad5f725befadbcef) `README: Remove reference to travis.`<br>
`*` [877bfc37](https://github.com/cbm755/octsympy/commit/877bfc370b9815ab25937387d0291f752816f8d7) `private/python_ipc_native: Import missing 'Str' sympy class.`<br>
`*` [cd263b38](https://github.com/cbm755/octsympy/commit/cd263b3853482226dbffe99486ff98c8445d6a63) `private/store_vars_in_python: Fix handling of 0x0 matrix.`<br>
`*` [2b697415](https://github.com/cbm755/octsympy/commit/2b69741513200c78b97b2a80794062d0c48b3c85) `CI: Enable pythonic tests.`<br>
`*` [9b4d7deb](https://github.com/cbm755/octsympy/commit/9b4d7debc78170ddf84985f00eb49fc75342de5a) `INDEX, NEWS: Add cumprod, cumsum and ztrans.`<br>
`*` [cc498e97](https://github.com/cbm755/octsympy/commit/cc498e97a809cbc401ddf3d1cc0bfbb945c915ce) `ztrans: Add new function.`<br>
`*` [fb5edb7f](https://github.com/cbm755/octsympy/commit/fb5edb7fcb7ee7db0cfaa5a6ba5db81e0f1a55ef) `CI: Workaround "fatal: unsafe repository" error.`<br>
`*` [e57a57d1](https://github.com/cbm755/octsympy/commit/e57a57d19309a2d8039bd1de640fa20f645cd712) `unique: Fix #1047 and add test case accordingly.`<br>

## Unmerged Commits

Unmerged commits are either from  `fixes-1145`, `wip-add-iztrans` or `wip-2d-sym` branch. To obtain them, one can simply clone the repository:

```
$ git clone https://github.com/alexvong1995/octsympy.git
```

and checkout the relevant branches.

After that, we can generate a graph of unmerged commits:

```
$ git log --graph --pretty=oneline \
      --decorate --decorate-refs=refs/remotes/origin --decorate-refs=refs/tags \
      --author='Alex Vong' --until=2022-09-12 \
      origin/fixes-1145 origin/wip-add-iztrans origin/wip-2d-sym \
      ^origin/main ^origin/Array_not_Matrix | \
      sed -e 's@\(.*\) \([0-9a-f]\{8\}\)\([0-9a-f]\{32\}\) \(.*\)@`\1` [\2](https://github.com/alexvong1995/octsympy/commit/\2\3) `\4`<br>@' \
          -e 's@^[^`][^`]*$@`&`<br>@'
```

`*` [94efa154](https://github.com/alexvong1995/octsympy/commit/94efa1543d4443dad5de0c4161223fdc1993e889) `(origin/wip-2d-sym) Python header: Add more 2D sym functions.`<br>
`*` [7ba93704](https://github.com/alexvong1995/octsympy/commit/7ba9370430173a2725cafd70ad6ac0e0208118f3) `Python header: Rename 'make_matrix_or_array' -> 'make_2d_sym'.`<br>
`*` [55d445ee](https://github.com/alexvong1995/octsympy/commit/55d445ee657b32673ae282ba294efa5d955c8c6e) `(origin/wip-add-iztrans) @sym/iztrans: Add new function.`<br>
`*` [be364a7d](https://github.com/alexvong1995/octsympy/commit/be364a7d9f57cbef638e4039f614814190dd3ac1) `(origin/fixes-1145) make_temp_dir__: New function.`<br>
`*` [47631041](https://github.com/alexvong1995/octsympy/commit/476310415a540abe3085cee48a35823eb668b634) `make_temp_file__: New function.`<br>

## What We Learnt

As an occasional contributor of free software projects without experience working on them full time in the past, it was a great opportunity to experience various aspects of being a frequent contributor. As a frequent contributor, one learns to balance between putting time into maintainance, developement, testing and releases. For examples, one cannot keep adding new features without fixing old bugs. Similarly, one has to look for regressions after doing a release and do a follow-up bug-fix release if necessary.

Besides, one learns that the bug tracker is a powerful tools for communicating ideas. It is often beneficial to file a bug report as soon as one hit a problem. After filing the bug report, if someone has seen the problem before, they can provide useful comment and resolve the bug. If not, the bug report can serve as a tracker for progress tracking. Not filing a bug report risks multiple developers working on the same issue without noticing.

One also learns how to review code written by others. As an occasional contributor in the past, code review was often done by others, usually by a frequent contributor. This time, as a frequent contributor, one wrote code as well as reviewed code. Code review is a important process for catching mistakes and improving the code. Different developers often have experience in different projects. The reviewer can sometimes utilise their unique perspective to improve the code.

## Challenges

Without prior experience working with travis CI or github action, we had to learned their syntax in order to port all pythonic tests from travis CI to github action. Fortunately, all these CIs basically use shell code for building by embedding them into a YAML file. In practice, since we know how to write shell script, we can learn the syntax of these CIs by reading the documentation and by guessing.

We discovered a number of security related issues due to limitations of the underlying language. For instance, in various places, a variable string is passed to the `system` function to call an executable. This potentially dangerous because given untrusted inputs, if the untrusted inputs can influence the variable string, it may cause `system` to execute unexpected commands. This is difficult to fix portably because the underlying language does not provide a portable way to call an executable without going through shell.

Similarly, race conditions exist in the creation of temporary files and directories. While one can add new functions to create temporary files and directories securely and portably, there are issues with this approach. If one were to create a private function, it cannot be called from tests. If one were to create a public function, it would pollute the public namespace. It is not clear what we should do.

Finally, when we first started with the *future* subproject, it was not clear where to start. We spent a week or two trying to fix test failures of the draft PR [#1194](https://github.com/cbm755/octsympy/pull/1194) without progress. Later, we figured out that we should add a helper function for constructing 2D sym and updated functions one by one without worrying too much about test failures.

## Future Plans

We now co-maintain octsympy after being invited to do so (see [#129](https://github.com/gnu-octave/packages/pull/129)). As a co-maintainer, we will fix bugs reported by users and gradually improve octsympy during spare time.

We will continue working on WIP PR [#1234](https://github.com/cbm755/octsympy/pull/1234), which adds the new function `@sym/iztrans`. This may take some time but it should be relatively straightforward since the function is already working and all we have to do is to add missing tests and documentation.

We also wish to continue what we did in the *future* subproject and completely get rid of non-`Expr` from `Array`. More concretely, we will keep creating and reviewing PRs which merge topic branch onto `Array_not_Matrix` until all tests pass. See *tracker issue for non-Expr in matrices* [#1124](https://github.com/cbm755/octsympy/issues/1124) for the current progress.

After that, we can proceed to investigate performance issue regarding code crossing the Octave-Python language boundary. Currently, we minimise such crossing due to performance concern. However, this is unsatisfactory because it creates the distinction between functions implemented in Octave only and functions implemented in both Octave and Python. This leaks implementation details and partially breaks the function abstraction. We should investigate whether creating a new experimental `xsym` class using the Pythonic IPC can solve this issue after we solve the performance issue of Pythonic IPC itself. There are other potential solutions as well, see [#1194](https://github.com/cbm755/octsympy/pull/1194) for details.

Finally, we should fix all security related issues mentioned in section [**Challenges**](./final-report.md#challenges). In case it is not possible to fix them in the short term, we should at least warn people by documenting them (see [#1143](https://github.com/cbm755/octsympy/issues/1143) and [#1144](https://github.com/cbm755/octsympy/issues/1144)) similar to what sympy did.

## Acknowledgement

We thank our mentor [Colin B. Macdonald](https://octave.discourse.group/u/cbm) ([@cbm755](https://github.com/cbm755)) for providing invaluable guidance, support, feedback and code review from the beginning to the end of this project in a timely fashion. Without them, this project would not have been possible. We thank GSoC 2016 participant Abhinav Tripathi ([@genuinelucifer](https://github.com/genuinelucifer)) for his important first step of bringing Pythonic to octsympy. We thank mentor [Kai Torben Ohlhus](https://octave.discourse.group/u/siko1056) ([@siko1056](https://github.com/siko1056)) for organising GSoC meetings and replying to our questions on the Octave forum. We thank all GSoC [participants](https://wiki.octave.org/Summer_of_Code#GSoC_2022) and [mentors](https://octave.discourse.group/g/gsoc_mentors) for listening and providing feedback during GSoC meetings. We thank GSoC for supporting us contributing to free software full time.
