# GSoC 2022 Symbolic package project proposal

## Synopsis

The symbolic package for Octave, octsympy, is a highly demanded package and an important project to the community. As such, it is important to keep it in a healthy state. To create a balanced project, we divide the project into 3 parts to deal with **present**, **past** and *future* goals respectively. The **present** part aims to work towards 3.0.0 milestones, such as supporting the latest versions of Sympy & Octave and improving the CI. The **past** part deals with long-standing bugs that had fallen through the cracks, fixing those lost and forgotten bugs. Finally, the *future* part will look at how we can improve maintainance going forward. We will try an experimental work based on the Pythonic project. That being said, exactly how far we get may depend on mentor guidance. In any case, our goal is to make octsympy easier to maintain in the future.

## Benefits to Community

As a free software replacement for SMT, octsympy is a highly demanded package and an important project to the community. It brings the the power of SymPy to Octave / Matlab. It is compatible with Octave / Matlab. While SymPy allows one to write CAS code in Python, octsympy allows one to write CAS code in Octave / Matlab.

Many widely used general-purpose CASs are non-free. Hence it is important to have free software general-purpose CASs in addition to (wx)Maxima for the community. Also, SymPy is modern and actively developed, instead of being on maintenance mode.

Finally, being a replacement for SMT compatible with both Octave & Matlab, octsympy can act as a gateway for attracting people to free software. After experiencing their usefulness and understanding how they are built, users could be inspired to become contributors in the future.

## Name and Contact information

I am Alex Vong. You can contact me by sending (PGP-encrypted) email to `alexvong1995 AT protonmail DOT com`. My PGP public key can be downloaded [here](https://notabug.org/alexvong1995/gpg-key-transition/raw/master/A5FCA28E0FF8E4C57F75555CDD66FE5CB79F5A5E.asc).

## TODO Deliverables

Please note that the following timeline is now outdated, see [updated timeline](./updated-timeline.md) for the latest timeline.

- (**Present**) [Week 1-4] work towards 3.0.0 milestones
    - [Week 1-2] either support or blacklist Sympy 1.9
        - investigate if Sympy 1.9 should be blacklisted (see [#1109](https://github.com/cbm755/octsympy/issues/1109))
        - support Sympy 1.9 if we do not blacklist it
            - run tests and doctests to check for regressions
            - fix all fixable regressions & mark all unfixable ones to `XFAIL`
            - enable CI for Sympy 1.9
    - [Week 1-2] support Sympy 1.10.1
        - run tests and doctests to check for regressions
        - fix all fixable regressions & mark all unfixable ones to `XFAIL`
        - enable CI for Sympy 1.10.1
    - [Week 3-4] improve existing CI
        - investigate if all travis CI tests were already ported to GitHub actions
        - port remaining tests if make sense
        - remove the travis CI file [#1072](https://github.com/cbm755/octsympy/issues/1072)
        - CI: update doctests matrix after heaviside fixes [#1103](https://github.com/cbm755/octsympy/issues/1103)
    - [Week 3-4] sympref test fails when in read-only directory [#1060](https://github.com/cbm755/octsympy/issues/1060)
        - create temporary files in `TMPDIR` instead of the current directory
    - [Week 3-4] output argument error handling change in Octave 7.1 [#1111](https://github.com/cbm755/octsympy/issues/1111)
        - adjust tests based on Octave version
    - [Week 4] release octsympy 3.0.0 if sufficient milestones are completed

- (**Past**) [Week 5-8] fix long-standing bugs
    - [Week 5-6] add cart2pol function and friends [#865](https://github.com/cbm755/octsympy/issues/865)
    - [Week 5-6] support sortrows [#362](https://github.com/cbm755/octsympy/issues/362)
    - [Week 5-6] adjoint function for numerical input [#1049](https://github.com/cbm755/octsympy/issues/1049)
    - [Week 7-8] ztrans [#853](https://github.com/cbm755/octsympy/issues/853)
        - use the series definition of [(unilateral) z-transform](https://en.wikipedia.org/wiki/Z-transform#Unilateral_Z-transform)
    - [Week 7-8] Piecewise Support [#582](https://github.com/cbm755/octsympy/issues/582)
    - [Week 8] fix additional bugs depending on our progress

- [Week 8] phase 1 report

- \<Optional\> (*Future*) [Week 9-12] make octsympy easier to maintain
    - [Week 9-10] disable `popen2` and `system` IPC mechanism to simplify debugging
    - [Week 9-10] create a new experimental `xsym` (classdef) class acting as a thin wrapper for underlying Sympy values
    - [Week 9-10] incrementally update functions to support `xsym` values
        - only octsympy functions and primitive `py.sympy` methods should be used when dealing with `xsym` values
        - `pycall_sympy__` should never be called when dealing with `xsym` values
    - [Week 11-12] investigate what is the best way to expose the full functionality of Sympy with less glue and create a proof-of-concept
        - we should focus on user extensibility and make it easy for users to define their own wrapper around Sympy functions, methods or constants
        - probably we need a way to get the underlying Sympy value out of a `xsym` value

- [Week 13] final report

## Related Work

In 2016, Abhinav Tripathi ([@genuinelucifer](https://github.com/genuinelucifer)) successfully re-implemented the communication layer using the Pythonic package (Pythonic was called PyTave back then) in [a GSoC project](https://genuinelucifer.wordpress.com/). We thank him for his important first step of bringing Pythonic to octsympy. Our optional *future* subproject is a continuation of his effort.
