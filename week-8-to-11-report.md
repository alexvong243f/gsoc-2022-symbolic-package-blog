# Week 8 to 11 Report

During the eighth to eleventh week, we release octsympy 3.0.1, continue fixing long-standing bugs and start investigating on how to get non-`Expr` out of `Matrix`. I attended:

- GSoC 2022: meeting @ Mon 2022-08-08
- GSoC 2022: meeting @ Mon 2022-08-22
  - discussed with mentor [@cbm755](https://github.com/cbm755) on how to implement non-`Matrix` 2D sym

and helped:

- complete 3.0.1 [milestones](https://github.com/cbm755/octsympy/milestone/11?closed=1)
  - finalise PR [#1195](https://github.com/cbm755/octsympy/pull/1195),
    which fixes *tempfile related problems on MXE windows build* [#1182](https://github.com/cbm755/octsympy/issues/1182) (regression introduced in 3.0.0)
  - create PR [#1199](https://github.com/cbm755/octsympy/pull/1199),
    which *Add missing 'apt-get update'*
  - create PR [#1200](https://github.com/cbm755/octsympy/pull/1200),
    which fixes *CI: test on Octave 7.2.0 and latest* [#1197](https://github.com/cbm755/octsympy/issues/1197)
  - create PR [#1201](https://github.com/cbm755/octsympy/pull/1201),
    which fixes *request more detail in 'sympref diagnose'* [#1009](https://github.com/cbm755/octsympy/issues/1009)

- do 3.0.1 [release](https://github.com/cbm755/octsympy/releases/tag/v3.0.1)
  - create PR [#1202](https://github.com/cbm755/octsympy/pull/1202),
    which *Release v3.0.1*
  - create ticket [#486](https://sourceforge.net/p/octave/package-releases/486/) *symbolic 3.0.1 release* to upload tarballs for review

- complete post-release tasks
  - create PR [#154](https://github.com/gnu-octave/packages/pull/154),
    which *Add new entry for 3.0.1 release*
  - create PR [#1205](https://github.com/cbm755/octsympy/pull/1205),
    which partially fixes *post-release bumps* [#1204](https://github.com/cbm755/octsympy/issues/1204)
  - create follow-up issue *Should the latest image be tagged with the `latest` tag?* [#11](https://github.com/gnu-octave/docker/issues/11)
  - create follow-up issue *Makefile: Compute and display SHA-256 of tarballs in `make release`* [#1208](https://github.com/cbm755/octsympy/issues/1208)
  - create follow-up issue *release: Sign release with GPG* [#1203](https://github.com/cbm755/octsympy/issues/1203)

- create and investigate *@sym/subsref: symbolic matrix subsref does not seem to work in m-file* [#1207](https://github.com/cbm755/octsympy/issues/1207)
- create PR [#1209](https://github.com/cbm755/octsympy/pull/1209),
  which partially fixes *add cart2pol function and friends* [#865](https://github.com/cbm755/octsympy/issues/865)

- investigate *tracker issue for non-Expr in matrices* [#1124](https://github.com/cbm755/octsympy/issues/1124)
  - investigate draft PR [#1194](https://github.com/cbm755/octsympy/pull/1194)
  - investigate *laplace test giving deprecation warning on SymPy > 1.9* [#1052](https://github.com/cbm755/octsympy/issues/1052)

From week 8 to week 11, we attended the GSoC meeting biweekly to discuss with mentors about tricky issues we encountered and to keep ourselves informed about the progress of all GSoC participants. We completed some 3.0.1 milestones, including an important regression introduced by the 3.0.0 release. After that, we released octsympy 3.0.1 and completed various post-release tasks as well. More importantly, we started working on the optional *future* subproject by investigating various issues and draft PRs related to getting non-`Expr` out of `Matrix`. While we continued our **past** subproject of fixing long standing bugs, we considered the *future* subproject to be more urgent because of sympy [active deprecations](https://docs.sympy.org/latest/explanation/active-deprecations.html#non-expr-objects-in-a-matrix). After discussing with mentor, we decided to focus our efforts on the *future* subproject rather than the **past**.

Next week, we will continue working on the *future* subproject. We expected we would not be able to get non-`Expr` out of `Matrix` entirely before the end of GSoC due to time constraints and the optional nature of this subproject. Nevertheless, we will create various PRs to make functions compatible with non-`Matrix` 2D sym, which will make it easier to accomplish our goal eventually. In addition, if opportunity arises, we will fix additional bugs and completed the **past** subproject.

## Progress Summary

Below is a summary of our progress:

- (**Past**) [Week 5-8] fix long-standing bugs
    - ~[Week 5-6] add cart2pol function and friends [#865](https://github.com/cbm755/octsympy/issues/865)~ (`@sym/cart2pol` added by me, the GSoC participant)
    - [Week 5-6] support sortrows [#362](https://github.com/cbm755/octsympy/issues/362)
    - ~[Week 5-6] adjoint function for numerical input [#1049](https://github.com/cbm755/octsympy/issues/1049)~
    - ~[Week 7-8] ztrans [#853](https://github.com/cbm755/octsympy/issues/853)~ (fixed by me, the GSoC participant, before GSoC had officially started)
        - ~use the series definition of [(unilateral) z-transform](https://en.wikipedia.org/wiki/Z-transform#Unilateral_Z-transform)~
    - [Week 7-8] Piecewise Support [#582](https://github.com/cbm755/octsympy/issues/582)
    - [Week 8] fix additional bugs depending on our progress

- \<Optional\> (*Future*) [Week 9-12] make octsympy easier to maintain
    - [Week 9-12] get non-expr out of matrix as much as possible since sympy deprecated the use of non-expr in matrix (see [#1124](https://github.com/cbm755/octsympy/issues/1124))
        - ~[Week 9-10] investigate draft PR [#1194](https://github.com/cbm755/octsympy/pull/1194)~
        - ~[Week 9-10] investigate *laplace test giving deprecation warning on SymPy > 1.9* [#1052](https://github.com/cbm755/octsympy/issues/1052)~
        - ~[Week 9-10] investigate *eq: giving TypeError: cannot determine truth value of Relational* [#1055](https://github.com/cbm755/octsympy/issues/1055)~ (fixed by mentor [@cbm755](https://github.com/cbm755))
        - [Week 11-12] fix *laplace test giving deprecation warning on SymPy > 1.9* [#1052](https://github.com/cbm755/octsympy/issues/1052)
        - [Week 11-12] fix *tracker issue for non-Expr in matrices* [#1124](https://github.com/cbm755/octsympy/issues/1124) if time allow

- [Week 13] final report
